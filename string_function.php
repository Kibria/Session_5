<?php

    //echo "How Are \"You\" ";

//$myStr = addslashes('Hello "31" Hello "31" Hello "31" Hello "31" ' );
//echo $myStr;

$myStr = addslashes('Hello \31\ Hello \'31\' Hello "31" Hello "31" ' );
echo $myStr."<br>";


$myStr = "Hello World! How is life?.<br>";
$myArr = explode(" ",$myStr);
print_r($myArr);
echo "<br>";

$myStr = implode(" * ",$myArr);
echo $myStr."<br>";


$myStr = '<br> means line break';
$myStr = htmlentities($myStr);
echo $myStr;


$myStr = "    Hello World          ";
echo trim($myStr);

$myStr = "      Hello World     ";
echo ltrim($myStr);

$myStr = "    Hello World           ";
echo rtrim($myStr);


$myStr = "\n\n\n\n\nHi There!";

echo nl2br($myStr);


echo "<br> <br>";
$mainStr = "Hello World";
$padStr = str_pad($mainStr,50,"*");
echo $padStr;


echo "<br> <br>";
$mainStr = "Hello World";
$repeatStr = str_repeat($mainStr,5);
echo $repeatStr;


echo "<br> <br>";
$mainStr = "Hello World";
$replaceStr = str_replace("o","O",$mainStr);
echo $replaceStr;


echo "<br> <br>";
$mainStr = "Hello World";
$myArr = str_split($mainStr);
print_r($myArr);


echo "<br> <br>";
$mainStr = "Hello World";
echo strlen($mainStr);


echo "<br> <br>";
$mainStr = "Hello World";
echo strtolower($mainStr);


echo "<br> <br>";
$mainStr = "Hello World";
echo strtoupper($mainStr);


echo "<br> <br>";
$mainStr = "Hello World";
$subStr = "Hello";
echo substr_compare($mainStr,$subStr,0);


echo "<br> <br>";
$mainStr = "Hello World Hello World Hello World Hello World";
echo substr_count($mainStr,"Hello");



echo "<br> <br>";
$mainStr = "Hello World Hello World Hello World Hello World";
$subStr = "Hi";
echo substr_replace($mainStr,$subStr,10);


echo "<br> <br>";
$mainStr = "hello world";
echo ucfirst($mainStr);


echo "<br> <br>";
$mainStr = "hello world";
echo ucwords($mainStr);